(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
    
     $('#MobileNavToggler').sidr({
      name: 'mobile-nav',
      source: '.nav-container nav, .secondary-nav-container'
    });

// Place any jQuery/helper plugins in here.
    
    // jQueryUI
    $( "input[type=submit], button" ).button();
    $( ":checkbox, :radio" ).checkboxradio();
    $("select").selectmenu();
    $( ".accordionize" ).accordion();
    $(".tabify").tabs();
    
    // jQueryUI Modal
    $( '#modal' ).dialog({
        buttons: [
            {
                text: "Ok",
                icons: {
                    primary: "ui-icon-heart"
                },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ],
        modal: true,
        autoResize:true,
        open: function () { $(this).load('modal.aspx'); },
        position: { my: 'center top', at: 'center top+20', of: window },
        title: 'Static Title Here',
        autoOpen: false,
        closeOnEscape: true
    });
    $( '.modal-trigger' ).on('click',function() {
        $( '#modal' ).dialog( 'open' );  // open the box
    });
    
}());

if ($('#vmap').length > 0) {
    $('#vmap').vectorMap({
        map: 'usa_en',
        backgroundColor: null,
        color: '#949494',
        hoverColor: '#cd040c',
        selectedColor: '#cd040c',
        enableZoom: true,
        showTooltip: true,
        enableZoom: false
    });
}
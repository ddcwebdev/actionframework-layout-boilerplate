<%@ Page Language="C#" Inherits="aspnetian.ScriptHtmlPage" EnableViewState="False" AutoEventWireup="False"  %>
<%@ Register Assembly="ASPNetInfrastructure" Namespace="DDCControls" TagPrefix="ddc" %>

<ddc:Define Client="VerizonAssetReport" OnFailure="error.aspx" AutoExecuteActions="False" Runat="Server" />
    
<!-- #include virtual="inc/global.aspx" -->
<!-- #include virtual="inc/openingmarkup.aspx" -->
    
    <section class="outer-container content-container">
        <article>
            <h2>Title</h2>
            <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
            <div class="flex-container row two-across">
                <div>
                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.</p>
                </div>
                <div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                </div>
            </div>            
        </article>
    </section>
    
<!-- #include virtual="inc/closingmarkup.aspx" -->
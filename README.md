### Introduction ###

In order to speed the development of products based on the Action Framework we have various website templates stored in CVS and, for the most part, they serve their purpose: to make development more efficient by providing quality code.

Where you run into problems is when the design you have to build looks nothing like the product template. The code is still quality, but trying to use it to implement a custom design is no more efficient than writing the code yourself. Worse yet, trying to rework template markup and CSS may end up costing you more time.

For times like those when all you want is a simple starting point where you can focus on quickly building the structure of your site, give this repository a whirl!

### What is this and what can I do with it? ###

The Action Framework Layout Boilerplate is a very small collection of files intended to give you a running start with any PSD breakdown. The two web pages provided show you how you can create basic yet common and versatile responsive layout structures such as:

* Two columns, 50/50 wide, of equal height
* Two columns, 66/33 wide, of equal height
* Three columns, 33/33/33 wide, of equal height
* Equally spaced main nav links
* Sticky footer
* Content "shelves"
* Content "tiles"

All of these things are accomplished through the use of combinations of inuitive, prewritten CSS classes. NO JAVASCRIPT!

Using these magical CSS classes you can turn this:


```
#!html

<div>
    <div>Left Column</div>
    <div>Right Column</div>
</div>
```

into a two-column layout easily by adding a couple of classes:


```
#!html

<div class="flex-container two-across">
    <div>Left Column</div>
    <div>Right Column</div>
</div>
```

The elements don't even have to be ``` div ``` tags. The parent could be a ``` ul ``` and the children could be ``` li ``` tags. This is similar to how the navigation is marked up in the sample files.

### This is great! What else is there? ###

Being an Action Framework template, this boilerplate is already broken down into header, footer and global includes and already contains the most basic DDC code required to get simple pages to load. You are still required to farm the appropriate standard product website template for functionality (or write those parts yourself). You also get the following:

* jQuery 3.1.1 - https://jquery.com/
* Modernizr 2.8.3 - https://modernizr.com/
* Font Awesome 4.7.0 - http://fontawesome.io/
* Normalize 5.0.0 - https://necolas.github.io/normalize.css/
* JQVMap 1.5.1 - https://jqvmap.com/
* Sidr 2.2.1 - https://www.berriart.com/sidr/
* jQueryUI 1.12.1 - https://jqueryui.com/

### jQueryUI ###

The build of jQueryUI included in this repository is not all-inclusive. Specific features were cherry-picked to keep things light.

Supported Interactions:

* Draggable
* Resizable

Supported Widgets:

* Accordions
* Buttons
* Checkboxradio
* Controlgroup
* Dialog
* Selectmenu
* Mouse (dependency)
* Menu (dependency)

Supported Effects:

None

If you're looking to use other features not listed here then you are welcome to go to http://jqueryui.com/download/ and compile your own build.

Finally, if you don't want to use jQueryUI at all you can remove it completely by deleting these files\paths:

* css\jquery-ui.*
* css\images
* js\vendor\jquery-ui.*

Don't forget to go into your js\plugins.js file and delete the jQueryUI-specific code, there.

### Credits ###

This layout template started life as a copy of the HTML5 Boilerplate project located here: https://html5boilerplate.com/

### Who can I thank for this awesome gift? ###

Please direct all praise to Larry Chavez. If you happen to have any questions he may be able to answer those, too.
<ddc:Execute Action="ShowUserInfo" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="DisplayNameFormat" Value="FirstNameLastName" Runat="Server" />
</ddc:Execute>

<ddc:Execute Action="Now" MustExecute="False" Runat="Server" />

<ddc:Comment Runat="Server">
	<!-- Get the current page's filename. -->
</ddc:Comment>

<ddc:Execute Action="Split.URL" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="String" Action="Input" Attr="CurrentURL" Runat="Server" />
	<ddc:ActionInput Name="DelimiterCharacters" Value="/" Runat="Server" />
</ddc:Execute>

<ddc:ForEach Action="Split.URL" Runat="Server">
	<ddc:If Action="Split.URL" Attr="Split" Runat="Server">
		<ddc:Block Condition="Regex" RegexOption="i" Value=".aspx" Runat="Server">
			<ddc:Variable Name="Filename" Runat="Server">
				<LongValue><ddc:Get Action="Split.URL" Attr="Split" Runat="Server" /></LongValue>
			</ddc:Variable>
		</ddc:Block>
	</ddc:If>
</ddc:ForEach>

<ddc:Execute Action="Split.Filename" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="String" Action="Input" Attr="Filename" Runat="Server" />
	<ddc:ActionInput Name="DelimiterCharacters" Value="." Runat="Server" />
</ddc:Execute>

<ddc:If Action="Split.Filename" Attr="Split" Runat="Server">
	<ddc:Block Value="default" IgnoreCase="True" Runat="Server">
		<ddc:Variable Name="CMCurrentFolder" Value="home" Runat="Server" />
	</ddc:Block>
	<ddc:Default Runat="Server">
		<ddc:Variable Name="CMCurrentFolder" Action="Split.Filename" Attr="Split" Runat="Server" />
	</ddc:Default>
</ddc:If>

<ddc:Comment Runat="Server">
	<!-- Set a variable with the content manager root folder's name. -->
</ddc:Comment>

<ddc:Variable Name="CMRootFolder" Value="RootFolderHere" Runat="Server" />

<ddc:Comment Runat="Server">
	<!-- Get the root's content (e.g. document title, client email addresses, main navigation, etc.). -->
</ddc:Comment>

<ddc:Execute Action="ListContents.Root" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="FolderPath" Action="Input" Attr="CMRootFolder" Runat="Server" />
</ddc:Execute>

<ddc:Comment Runat="Server">
	<!-- Build the "FolderPath" string to pull content for the current page. -->
</ddc:Comment>

<ddc:Execute Action="Concatenate.FolderPath" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="String1" Action="Input" Attr="CMRootFolder" Runat="Server" />
	
	<ddc:If Action="Input" Attr="CMCurrentFolder" Runat="Server">
		<ddc:Block Value="content" Runat="Server">
			<ddc:ActionInput Name="String2" Value="ContentPages" Runat="Server" />
			<ddc:ActionInput Name="String3" Action="Input" Attr="page" Runat="Server" />
		</ddc:Block>
		<ddc:Block Value="thankyou" Runat="Server">
			<ddc:ActionInput Name="String2" Value="ThankYou" Runat="Server" />
			<ddc:ActionInput Name="String3" Action="Input" Attr="from" Runat="Server" />
		</ddc:Block>
		<ddc:Default Runat="Server">
			<ddc:ActionInput Name="String2" Action="Input" Attr="CMCurrentFolder" Runat="Server" />
		</ddc:Default>
	</ddc:If>
	
	<ddc:ActionInput Name="Separator" Value="/" Runat="Server" />
</ddc:Execute>

<ddc:Execute Action="ListContents" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="FolderPath" Action="Concatenate.FolderPath" Attr="ConcatenatedString" Runat="Server" />
</ddc:Execute>

<ddc:Execute Action="Replace.mergeCopyrightYear" MustExecute="False" Runat="Server">
    <ddc:ActionInput Name="String" Runat="Server">
        <LongValue><ddc:Get Action="ListContents.Root" Attr="Content" OutputName="FinePrint" Runat="Server" /></LongValue>
    </ddc:ActionInput>
    <ddc:ActionInput Name="SubString" Value="[#YEAR#]" Runat="Server" />
    <ddc:ActionInput Name="ReplacementString" Runat="Server">
        <LongValue><ddc:Get Action="Now" Attr="Now" FormatAs="DateTime" FormatString="yyyy" Runat="Server" /></LongValue>
    </ddc:ActionInput>
</ddc:Execute>

<ddc:Comment Runat="Server">
	<!-- Get the error message content using an available error code. -->
</ddc:Comment>

<ddc:Execute Action="ShowContentRow.Error" MustExecute="False" Runat="Server">
	<ddc:ActionInput Name="FolderPath" Runat="Server">
		<LongValue><ddc:Get Action="Input" Attr="CMRootFolder" Runat="Server" />/Errors</LongValue>
	</ddc:ActionInput>
	<ddc:ActionInput Name="ContentName" Runat="Server">
        <LongValue><ddc:Get Action="Input" Attr="varError" Runat="Server"><OnEmpty>Default</OnEmpty></ddc:Get></LongValue>
    </ddc:ActionInput>
</ddc:Execute>
<ddc:ClearVariable name="varError" Runat="Server" />
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><ddc:Get Action="ListContents.Root" Attr="Content" OutputName="SiteName" Runat="Server" /><ddc:Get Action="ListContents" Attr="Content" OutputName="DocTitle" Runat="Server"><Prefix> - </Prefix></ddc:Get></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/normalize.css">   
        <link rel="stylesheet" href="js/vendor/jqvmap/jqvmap.css">  
        <link rel="stylesheet" href="css/font-awesome.min.css">   
        <link rel="stylesheet" href="css/main.css">        
        <link rel="stylesheet" href="css/jquery.sidr.css">
        <link rel="stylesheet" href="css/jquery-ui.min.css"> 
        <link rel="stylesheet" href="css/jquery-ui.structure.min.css"> 
        <link rel="stylesheet" href="css/jquery-ui.theme.css">         

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery-ui.min.js"></script>
        <script src="js/vendor/jqvmap/jquery.vmap.min.js"></script>
        <script src="js/vendor/jqvmap/maps/jquery.vmap.usa.js"></script>
        <script src="js/vendor/jquery.sidr.min.js"></script>
        <script src="js/main.js"></script>
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body id="<ddc:Get Action=Input Attr=CMCurrentFolder Runat=Server/>">
        <section class="header-container">
            <header class="clearfix flex-container row">
                <div class="mobile shrink-wrapped">
                    <a id="MobileNavToggler" href="#MainMenu"><i class="fa fa-bars" aria-hidden="true"></i></a>
                </div>
                <div class="brand-container">
                    <div><a href="default.aspx" class="logo" border="0">Logo Here</a></div>
                </div>
                <div class="secondary-nav-container">                    
                    <ul class="flex-container menu-style">
                        <li>
                            <a href="#">Go Here</a>
                            <ul>
                                <li><a href="#">Submenu Item 1</a></li>
                                <li><a href="#">Submenu Item 1</a></li>
                                <li><a href="#">Submenu Item 1</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Go There</a>
                            <ul>
                                <li><a href="#">Submenu Item 1</a></li>
                                <li><a href="#">Submenu Item 1</a></li>
                                <li><a href="#">Submenu Item 1</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Go Elsewhere</a>
                            <ul>
                                <li><a href="#">Submenu Item 1</a></li>
                                <li><a href="#">Submenu Item 1</a></li>
                                <li><a href="#">Submenu Item 1</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="mobile shrink-wrapped">
                    <a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </div>
            </header>
        </section>

        <section class="nav-container">
            <nav>
                <ul class="flex-container menu-style">
                    <li>
                        <a href="#">Home</a>
                        <ul>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">About Us</a>
                        <ul>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Our Products</a>
                        <ul>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Support</a>
                        <ul>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Contact Us</a>
                        <ul>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                            <li><a href="#">Submenu Item 1</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </section>
        <form runat="server">
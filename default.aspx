<%@ Page Language="C#" Inherits="aspnetian.ScriptHtmlPage" EnableViewState="False" AutoEventWireup="False"  %>
<%@ Register Assembly="ASPNetInfrastructure" Namespace="DDCControls" TagPrefix="ddc" %>

<ddc:Define Client="VerizonAssetReport" OnFailure="error.aspx" AutoExecuteActions="False" Runat="Server" />
    
<!-- #include virtual="inc/global.aspx" -->
<!-- #include virtual="inc/openingmarkup.aspx" -->
    
    <section class="banner-container">
        <article class="vertically-center">
            <h1>Banner or Carousel</h1>
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>Two-Column Layout</h2>
            <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
            <div class="flex-container two-across">
                <div>
                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.</p>
                </div>
                <div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                </div>
            </div>            
        </article>
        
        <article>
            <h2>Two-Column Layout with Spacing</h2>            
            <div class="flex-container two-across space-between">
                <div>
                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.</p>
                </div>
                <div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                </div>
            </div>            
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>Two-Column Layout Variation and Three-Column Layout</h2>
            <p>Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence on cross-platform integration.</p>
            <div class="flex-container">
                <div class="column-twothirds custom-class-one">
                    <div id="vmap"></div>
                </div>
                <div class="column-onethird custom-class-two">
                    <p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
                </div>
            </div>            
        </article>
        
        <article>
            <h2>Three Equal-Width Content Wells</h2>
            <div class="flex-container three-across">
                <div><p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p></div>
                <div><p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.</p></div>
                <div><p>Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains. Dramatically engage top-line web services vis-a-vis cutting-edge deliverables.</p></div>
            </div>
        </article>
        
        <article>
            <h2>Same As Above, Except Spaced Apart</h2>
            <div class="flex-container three-across space-between">
                <div><p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p></div>
                <div><p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.</p></div>
                <div><p>Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains. Dramatically engage top-line web services vis-a-vis cutting-edge deliverables.</p></div>
            </div>
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>Optional Aside Element: With an Aside</h2>
            <div class="flex-container twothirds-optional-aside">
                <div>
                    <p>This is the main content.</p>
                </div>
                <aside>
                    <p>This is an aside.</p>
                </aside>
            </div>
        </article>
        
        <article>
            <h2>... Spaced Apart</h2>
            <div class="flex-container twothirds-optional-aside space-between">
                <div>
                    <p>This is the main content.</p>
                </div>
                <aside>
                    <p>This is an aside.</p>
                </aside>
            </div>
        </article>
        
        <article>
            <h2>Optional Aside Element: Without an Aside</h2>
            <div class="flex-container twothirds-optional-aside">
                <div>
                    <p>This is the main content.</p>
                </div>
            </div>
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>Tiles</h2>
            <p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.</p>
            <div class="flex-container two-across tiles">
                <div class="vertically-center"><p>Ipsum</p></div>
                <div class="vertically-center"><p>Nullam</p></div>
                <div class="vertically-center"><p>Diam</p></div>
                <div class="vertically-center"><p>Lorem</p></div>
                <div class="vertically-center"><p>Quis</p></div>
                <div class="vertically-center"><p>Tristis</p></div>
                <div class="vertically-center"><p>Dolor</p></div>
            </div>
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>Standard and jQueryUI Form Controls</h2>
            <div class="flex-container two-across">
                <div>
                    <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
                </div>
                <div>
                    <fieldset>
                        <legend>Your Name</legend>
                        <div><label for="firstname">* First Name: </label><input id="firstname" type="text" required="required" /></div>
                        <div><label for="middlename">Middle Name: </label><input id="middlename" type="text" /></div>
                        <div><label for="lastname">* Last Name: </label><input id="lastname" type="text" required="required" /></div>
                    </fieldset>
                    <fieldset>
                        <legend>Your Address</legend>
                        <div><label for="street">Street Address: </label><input id="street" type="text" /></div>
                        <div><label for="city">City: </label><input id="city" type="text" /></div>
                        <div><label for="state">State: </label><select id="state"><option disabled></option><option value="DC">District of Columbia</option><option value="MD">Maryland</option><option value="VA">Virginia</option></select></div>
                        <div><label for="zip">* Zip: </label><input id="zip" pattern="\d{5}?" type="text" required="required" /></div>
                    </fieldset>
                    <fieldset>
                        <legend>Your Contact Information</legend>
                        <div><label for="emailaddress">* Email Address: </label><input id="emailaddress" type="email" required="required" /></div>
                        <div><label for="telephonenumber">Telephone Number: </label><input id="telephonenumber" type="tel" /></div>
                    </fieldset>
                    <fieldset class="inline-divs">
                        <legend>Favorite Ice Cream</legend>
                        <div>
                            <label for="radio1">
                                <input type="radio" id="radio1" name="icecream" value="Chocolate" />Chocolate
                            </label>
                        </div>
                        <div>
                            <label for="radio2">
                                <input type="radio" id="radio2" name="icecream" value="Strawberry" />Strawberry
                            </label>
                        </div>
                        <div>
                            <label for="radio3">
                                <input type="radio" id="radio3" name="icecream" value="Vanilla" />Vanilla
                            </label>
                        </div>
                    </fieldset>
                    <fieldset class="inline-divs">
                        <legend>Possessions</legend>
                        <div>
                            <label for="check1">
                                <input type="checkbox" id="check1" value="Car" />Car
                            </label>
                        </div>
                        <div>
                            <label for="check2">
                                <input type="checkbox" id="check2" value="House" />House
                            </label>
                        </div>
                        <div>
                            <label for="check3">
                                <input type="checkbox" id="check3" value="Boat" />Boat
                            </label>
                        </div>
                    </fieldset>
                    <p>Please review all of your information before submitting this form.</p>
                    <input type="submit" value="&#xf1d9; Send It!">
                </div>
            </div>
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>jQueryUI: Accordion</h2>
            <div class="accordionize">
                <h3>Section 1</h3>
                <div>
                    <p>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.</p>
                </div>
                <h3>Section 2</h3>
                <div>
                    <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.</p>
                </div>
                <h3>Section 3</h3>
                <div>
                    <p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.</p>
                    <ul>
                        <li>List item one</li>
                        <li>List item two</li>
                        <li>List item three</li>
                    </ul>
                </div>
                <h3>Section 4</h3>
                <div>
                    <p>Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.</p>
                    <p>Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                </div>
            </div>
        </article>
    </section>
    <section class="content-container">
        <article>
            <h2>jQueryUI: Modal</h2>
            <p>A simple on-click listener attached to <a href="javascript:" class="modal-trigger">this link</a> will open a jQueryUI modal dialog. This implementation uses an ajax call to load content from modal.aspx into the dialog, but you can just as easily load local content present in the current file (it just stays hidden until the modal is invoked).</p>
            <div id="modal"></div>
        </article>
    </section>
    
    <section class="content-container">
        <article>
            <h2>jQueryUI: Tabs</h2>
            <div class="tabify">
                <ul>
                    <li><a href="#tabs-1">Nunc tincidunt</a></li>
                    <li><a href="#tabs-2">Proin dolor</a></li>
                    <li><a href="#tabs-3">Aenean lacinia</a></li>
                </ul>
                <div id="tabs-1">
                    <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
                </div>
                <div id="tabs-2">
                    <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
                </div>
                <div id="tabs-3">
                    <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
                    <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
                </div>
            </div>
        </article>
    </section>
    
<!-- #include virtual="inc/closingmarkup.aspx" -->